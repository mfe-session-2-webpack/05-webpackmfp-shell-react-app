const path = require('path');

module.exports = {
  output: {
    clean: true,
  },
  devServer: {
    static: './dist',
  },
};
