import React from 'react';
import { Routes, Route } from 'react-router-dom';

import Home from './home/Home';
import DashboardRemote from './dashboard/DashboardRemote';
import VueApp from './vue-app/VueApp';
import Gallery from './gallery/Gallery';
import SimpleAppBar from './app-bar/SimpleAppBar';

function App() {
  return (
    <div className="App">
      <SimpleAppBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="dashboard" element={<DashboardRemote />} />
        <Route path="vueapp" element={<VueApp />} />
        <Route path="gallery" element={<Gallery />} />
      </Routes>
    </div>
  );
}

export default App;
