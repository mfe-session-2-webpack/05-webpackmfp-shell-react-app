import React from 'react';
import Skeleton from '@mui/material/Skeleton';

const RemoteDashboard = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import('RemoteReact/RemoteDashboard')), 5000);
  });
});

export default function DashboardRemote() {
  return (
    <>
      <React.Suspense
        fallback={<Skeleton variant="rectangular" width="100%" height={210} />}
      >
        <div>
          <RemoteDashboard />
        </div>
      </React.Suspense>
    </>
  );
}
