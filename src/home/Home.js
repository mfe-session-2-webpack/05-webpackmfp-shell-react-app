import React from 'react';
import { Link } from 'react-router-dom';
import Typography from '@mui/material/Typography';

function Home() {
  return (
    <>
      <Typography variant="h3" gutterBottom>
        Home Page
      </Typography>
      <nav>
        <Link to="/dashboard">
          <Typography variant="h6" gutterBottom>
            Dashboard
          </Typography>
        </Link>
      </nav>
      <nav>
        <Link to="/vueapp">
          <Typography variant="h6" gutterBottom>
            Vue app
          </Typography>
        </Link>
      </nav>
      <nav>
        <Link to="/gallery">
          <Typography variant="h6" gutterBottom>
            Gallery
          </Typography>
        </Link>
      </nav>
    </>
  );
}

export default Home;
