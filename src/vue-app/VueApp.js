import React from 'react';
import Skeleton from '@mui/material/Skeleton';

const AppVue = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import('./VueAppMount')), 5000);
  });
});

export default function VueApp() {
  return (
    <>
      <React.Suspense
        fallback={<Skeleton variant="rounded" width={600} height={210} />}
      >
        <div>
          <AppVue />
        </div>
      </React.Suspense>
    </>
  );
}
