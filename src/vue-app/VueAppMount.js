import { mount } from 'RemoteVue/AppRemoteVue';
import React, { useEffect } from 'react';

export default function VueAppMount() {
  useEffect(() => {
    mount(document.getElementById('vue-app'));
  }, []);

  return <div id="vue-app"></div>;
}
